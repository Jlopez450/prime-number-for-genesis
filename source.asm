	include "ramdat.asm"
		      dc.l $000000,   start,     ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.b "DON'T LOOK AT THIS!", $00
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l HBlank,    ErrorTrap, VBlank,    ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.b "SEGA MEGASIS    "
              dc.b "(C)2018.OCT JAL "
              dc.b "Number masher                                   "
              dc.b "Number masher                                   "
              dc.b "GM 01234567-89"
              dc.w $DEAD        ;checksum
              dc.b "J               "
              dc.l 0
              dc.l ROM_End
              dc.l $FF0000
              dc.l $FFFFFF
              dc.b "    "
              dc.b "    "
              dc.b "    "
              dc.b "            "                           
              dc.b "This program contains blast processing! "
              dc.b "JUE             "
start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq no_tmss             ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.w #$2700, sr       ;disable interrupts
		bsr clear_regs   ;make all registers zero
		bsr clear_ram    ;clear out RAM
        bsr setup_vdp
		bsr clear_vram   ;clear out video memory	
		bsr region_check
		lea (font),a5    ;ASCII font
		move.w #$07FF,d4 ;Graphics length in words
		move.l #$40000000,(a3);write to VRAM $0000
		bsr vram_loop
		move.l #$c0000000,(a3);write to color memory
		move.w #$0000,(a4) ;black
		move.w #$0000,(a4) ;black
		move.w #$0000,(a4) ;black
		move.w #$0EEE,(a4) ;white	
		lea (text1), a5
		move.l #$60800000,(a3)
		bsr message_loop
		lea (text2), a5
		move.l #$6d000000,(a3)
		bsr message_loop
		lea (text3), a5
		move.l #$66180000,(a3)
		bsr message_loop
		lea (text4), a5
		move.l #$64980000,(a3)
		bsr message_loop
		move.w #$2300,sr
		move.l #$01,d0;optimization - only divide odd numbers (dividend starts here)
loop:
		add.w #02,d0 ;optimization - skip even numbers
		bsr calc	
		bra loop 

calc:
		move.l d0,d2	;divide dividend by itself*	
		lsr.w #$01,d2	;optimization: Set divisor to 1/2 of dividend
		or.w #$0001,d2	;even numbers not allowed
		move.l d0,d4
		lsr.w #$02,d4	;optimization: Only divide by odd numbers and start at 1/2 of dividend (must quarter # of loops)
		sub.w #$02,d4	;Run (dividend-3) loops to not attempt divide by one, or zero. DBF adds 1 uncounted loop
calcloop:
		sub.l #$02,d2	;*minus 2 (and subtract divisor by two after each attempt) - skip even numbers
		move.l d0,d1
		divu.w d2,d1	;King Dividitron: "slayer of CPU cycles".
		swap d1			;half of DWORD with remainder part
		tst.w d1		;does remainder part have remainder?
		beq return		;no remainder = not prime
		dbf d4, calcloop
		bsr convert		;display found prime number
return:
		rts
	
;set-up stuff below this line	
;-----------------------------------------------------	

setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts  
		
vram_Loop:
        move.w (a5)+,(a4)
        dbf d4,vram_Loop
        rts  
		
message_loop:
		move.b (a5)+,d4	
		cmpi.b #$24,d4			;$ (end of text flag)
		beq return
		andi.w #$00ff,d4
        move.w d4,(a4)		
		bra message_loop	
		
clear_regs:
		moveq #$00000000,d0
		moveq #$00000000,d1
		moveq #$00000000,d2
		moveq #$00000000,d3
		moveq #$00000000,d4
		moveq #$00000000,d5
		moveq #$00000000,d6
		moveq #$00000000,d7
		move.l d0,a0
		move.l d0,a1
		move.l d0,a2
		move.l d0,a3
		move.l d0,a4
		move.l d0,a5
		move.l d0,a6
		rts
		
clear_ram:
		move.w #$7FF0,d0
		move.l #$FF0000,a0	
clear_ram_loop:
		move.w #$0000,(a0)+
		dbf d0, clear_ram_loop
		rts
		
clear_vram:
		move.l #$40000000,(a3)
		move.w #$7fff,d4
vram_clear_loop:
		move.w #$0000,(a4)
		dbf d4, vram_clear_loop
		rts

ErrorTrap:        
		move.w #$666,d6
		jmp start
        bra ErrorTrap

HBlank:
        rte

VBlank:
		bsr clock
		move.l #$65220000,(a3)	
		move.b minutes, d5
		andi.b #$f0, d5
		lsr.b #$04, d5
		add.b #$30,d5
		andi.w #$00ff,d5	
		move.w d5, (a4)
		move.b minutes, d5
		andi.b #$0f, d5
		add.b #$30,d5
		andi.w #$00ff,d5
		move.w d5, (a4)	
		move.w #$003a, (a4)		
		move.b seconds, d5
		andi.b #$f0, d5
		lsr.b #$04, d5
		add.b #$30,d5
		andi.w #$00ff,d5		
		move.w d5, (a4)
		move.b seconds, d5
		andi.b #$0f, d5
		add.b #$30,d5
		andi.w #$00ff,d5
		move.w d5, (a4)	
        rte
		
	include "clock.asm"
	include "hex2bcdV2.asm"

font:
	incbin "ascii.bin"
	;incbin "hexascii.bin"
	
text1:
	dc.b "---ComradeOj's prime number generator---$ "
text2:
	dc.b " Compiled: OCT/03/2018        mode5.net$"
text3:
	dc.b "Prime number:$"
text4:
	dc.b "Time elapesd:$"
	
VDPSetupArray:
	dc.w $8014		
	dc.w $8164  ;Genesis mode, DMA disabled, VBLANK-INT enabled		
	dc.w $8208	;field A at $2000    
	dc.w $8300
	dc.w $8401	;field B at $2000	
	dc.w $8518	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8AFF	;no H-int
	dc.w $8B00		
	dc.w $8C81	
	dc.w $8D34		
	dc.w $8E00
	dc.w $8F02	;auto increment	(should almost always be 2)
	dc.w $9001		
	dc.w $9100		
	dc.w $9200

ROM_End:
              
              